﻿using Stringette.Stringettes;
using Stringette.Test.Content;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Stringette.Program
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteHeader();

            TestAllContents(Contents.Shakespeare,
                Contents.Strindberg,
                Contents.WarAndPeace,
                Contents.Miserables,
                Contents.LeonardoDaVinci,
                Contents.KingJamesBible,
                Contents.Iliad,
                Contents.FrithiofsSaga,
                Contents.DavidCopperfield,
                Contents.Chinese,
                Contents.Arabic,
                Contents.Bibeln,
                Contents.Greek,
                Contents.Japanese,
                Contents.Russian,
                Contents.Tegner);

            Console.WriteLine("==============================");
            TestPoems(Contents.Tegner, 10);
            TestPoems(Contents.Tegner, 100);
            TestPoems(Contents.Tegner, 1000);
            TestPoems(Contents.Tegner, 10000);
            TestPoems(Contents.Tegner, 100000);

            Console.WriteLine("==============================");
            TestPoems(Contents.Tegner, 10, true);
            TestPoems(Contents.Tegner, 100, true);
            TestPoems(Contents.Tegner, 1000, true);
            TestPoems(Contents.Tegner, 10000, true);
            TestPoems(Contents.Tegner, 100000, true);

            Console.WriteLine("==============================");
            TestIndexOf(Contents.Tegner, 10000, false, StringComparison.Ordinal, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
            TestIndexOf(Contents.Tegner, 10000, false, StringComparison.OrdinalIgnoreCase, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
            TestIndexOf(Contents.Tegner, 10000, false, StringComparison.InvariantCulture, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
            TestIndexOf(Contents.Tegner, 10000, false, StringComparison.InvariantCultureIgnoreCase, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
            TestIndexOf(Contents.Tegner, 10000, false, StringComparison.CurrentCulture, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
            TestIndexOf(Contents.Tegner, 10000, false, StringComparison.CurrentCultureIgnoreCase, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");

            TestIndexOf(Contents.Tegner, 10000, true, StringComparison.Ordinal, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
            TestIndexOf(Contents.Tegner, 10000, true, StringComparison.OrdinalIgnoreCase, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
            TestIndexOf(Contents.Tegner, 10000, true, StringComparison.InvariantCulture, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
            TestIndexOf(Contents.Tegner, 10000, true, StringComparison.InvariantCultureIgnoreCase, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
            TestIndexOf(Contents.Tegner, 10000, true, StringComparison.CurrentCulture, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
            TestIndexOf(Contents.Tegner, 10000, true, StringComparison.CurrentCultureIgnoreCase, "svenska stålet", "segerfana", "Ärans och hjältarnas språk!");
        }

        private static void TestAllContents(params string[] contents)
        {
            foreach(var content in contents)
            {
                TestBookRows(content, false);
                TestBookTotal(content, false);
                TestBookRows(content, true);
                TestBookTotal(content, true);
            }
        }
        
        private static void TestIndexOf(string name, int count, bool parallel, StringComparison stringComparison, params string[] indexOfStrings)
        {
            var stringContent = Contents.Load(name);
            Stringette stringette = Stringette.New(stringContent);

            Stopwatch stopWatch = Stopwatch.StartNew();
            long totalIndextringette = 0;

            ForEach(Enumerable.Range(0, count), parallel, (i) =>
            {
                foreach (string indexOf in indexOfStrings)
                {
                    Interlocked.Add(ref totalIndextringette, stringette.IndexOf(indexOf));
                }
            });

            long stringetteMs = stopWatch.ElapsedMilliseconds;

            stopWatch = Stopwatch.StartNew();
            long totalIndexString = 0;
            ForEach(Enumerable.Range(0, count), parallel, (i) =>
            {
                foreach (string indexOf in indexOfStrings)
                {
                    Interlocked.Add(ref totalIndexString, stringContent.IndexOf(indexOf, 0, stringComparison));
                }
            });
            
            long stringMs = stopWatch.ElapsedMilliseconds;

            Console.WriteLine($"IndexOf * {indexOfStrings.Length} * {count}: {stringetteMs} ms {totalIndextringette} (string {stringMs} ms {totalIndexString} {stringComparison.ToString()})");
        }
        
        private static void TestPoems(string name, int count, bool parallel = false)
        {
            TestBookRaw((n) =>
            {
                string loadedContent = Contents.Load(name);
                List<string> strings = new List<string>();
                for (int i = 0; i < count; i++)
                {
                    strings.Add($"{i+1}. {loadedContent}");
                }

                return strings;
            }, name, parallel);
        }

        public static void TestBookRows(string name, bool parallel = false)
        {
            TestBookRaw((n) => Contents.Load(n, 0), name, parallel);
        }

        public static void TestBookTotal(string name, bool parallel = false)
        {
            TestBookRaw((n) => new List<string> { Contents.Load(n) }, name, parallel);
        }

        public static void TestBookRaw(Func<string, List<string>> loadfunc, string name, bool parallel = false)
        {
            GlobalIndexingCompactStringette.CharIndex.Reset();

            //Load the string and test how much memory they consume
            List<string> strings = loadfunc(name);
            long stringMemory = GetCurrentMemoryConsumption();
            int uniqueChars = new HashSet<char>(strings.SelectMany(s => s)).Count;

            //Create the stringettes out of the strings and time it and test memory consumption
            Stopwatch stopWatch = Stopwatch.StartNew();
            List<Stringette> stringettes = CreateStringettes(strings, parallel);
            strings = null;
            long createTime = stopWatch.ElapsedMilliseconds;
            long stringetteMemory = GetCurrentMemoryConsumption();

            //Make the stringettes strings again and se how long it takes
            long materializeTime = MaterializeStringettes(stringettes, parallel);

            WriteReport(name, stringettes.Count, uniqueChars, stringMemory, stringetteMemory, createTime, materializeTime, parallel);
        }

        private static long GetCurrentMemoryConsumption()
        {
            GC.Collect();
            GC.Collect();
            GC.Collect();

            return GC.GetTotalMemory(false);
        }

        private static long MaterializeStringettes(List<Stringette> stringettes, bool parallel)
        {
            Stopwatch watch = Stopwatch.StartNew();

            ForEach(stringettes, parallel, (s) =>
            {
                s.ToString();
            });

            return watch.ElapsedMilliseconds;
        }

        private static void WriteHeader()
        {
            Console.WriteLine("{0,-20}{1,12}{2,16}{3,16}{4,16}{5,20}{6,15}{7,17}{8,12}", "Work", "Gain(%)", "String count", "Unique chars", "String bytes", "Stringette bytes", "In time(ms)", "Out time (ms)", "Parallel");
        }

        private static void WriteReport(string name, int numberOfStrings, int uniqueChars, long stringMemory, long stringetteMemory, long buildTimeMs, long materializeTimeMs, bool parallel)
        {
            int percent = (int)((1.0d - ((double)stringetteMemory / stringMemory)) * 100);
            Console.WriteLine("{0,-20}{1,12}{2,16}{3,16}{4,16}{5,20}{6,15}{7,17}{8,12}", name, percent, numberOfStrings, uniqueChars, stringMemory, stringetteMemory, buildTimeMs, materializeTimeMs, parallel);
        }

        public static List<Stringette> CreateStringettes(List<string> strings, bool parallel)
        {
            ConcurrentQueue<Stringette> bag = new ConcurrentQueue<Stringette>();
            ForEach(strings, parallel, (s) =>
            {
                bag.Enqueue(Stringette.New(s));
            });

            return bag.ToList();
        }

        private static void ForEach<T>(IEnumerable<T> items, bool parallel, Action<T> work)
        {
            if (parallel)
            {
                Parallel.ForEach(items, (i) =>
                {
                    work(i);
                });
            }
            else
            {
                foreach (T i in items)
                {
                    work(i);
                }
            }
        }
    }
}