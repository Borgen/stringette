# Stringette

Stringette is a class written in C# allowing for fast and compact storage of large strings in memory. In tests I've measured up to 56% lower memory footprint compared to a regular System.String. The Stringette is also faster when calling basic string methods such as IndexOf. A drawback is the time required for creating the object.

The class utilizes the fact that strings often do not use characters from the entire UTF16 spectrum, but more often, only tens of distinct characters. For example: The entire bibliography of Shakespeare uses a little over 90 unique characters. Including upper and lower case letters.

As these 90 characaters can be stored using 7 bits, we can save just over 50% used memory compared to System.String, which uses 16 bits per character. Instead of pointing to a certain character in UTF16, the value points to an index of the character in a global character collection.

To be able to squeeze as many bits as possible out of the memory, these indecies are compacted into an array of uints using some bitshifting magic!

## How to use?

To make a string into a Stringette simply call

    Stringette str = Stringette.New(stringVariable);

And to get the string back

    string stringValue = str.ToString();
	
The class supports features such as Contains, IndexOf and Substring.
