﻿using System;
using NUnit.Framework;
using Stringette.Test.Content;
using System.Collections.Generic;
using System.Threading;
using Microsoft.SqlServer.Server;
using Stringette.Stringettes;

namespace Stringette.Test
{
    [TestFixture]
    public class StringetteTests
    {
        [SetUp]
        public void Setup()
        {
            GlobalIndexingCompactStringette.CharIndex.Reset();
        }

        [TestCase("This is a string")]
        [TestCase("This Is A Nother String")]
        [TestCase("asdasfgfdgdfgertrexcvxcgxvksdfjdsfweuwrweirpofdnbvxcncbvxhcbu8234723484+96+5465 75 0374 0358539454")]
        public void StringMatch(string value)
        {
            Stringette stringette = Stringette.New(value);
            Assert.AreEqual(value, stringette.ToString());
        }

        [TestCase(Contents.Strindberg)]
        [TestCase(Contents.Shakespeare)]
        [TestCase(Contents.Miserables)]
        [TestCase(Contents.KingJamesBible)]
        [TestCase(Contents.FrithiofsSaga)]
        [TestCase(Contents.Chinese)]
        [TestCase(Contents.DavidCopperfield)]
        [TestCase(Contents.LeonardoDaVinci)]
        [TestCase(Contents.WarAndPeace)]
        [TestCase(Contents.Tegner)]
        [TestCase(Contents.Iliad)]
        [TestCase(Contents.Arabic)]
        [TestCase(Contents.Bibeln)]
        [TestCase(Contents.Greek)]
        [TestCase(Contents.Japanese)]
        [TestCase(Contents.Russian)]
        public void IntegrityTotal(string name)
        {
            string rawstring = Contents.Load(name);
            Stringette stringette = Stringette.New(rawstring);
            Assert.AreEqual(rawstring, stringette.ToString());
        }

        [TestCase(Contents.Strindberg)]
        [TestCase(Contents.Shakespeare)]
        [TestCase(Contents.Miserables)]
        [TestCase(Contents.KingJamesBible)]
        [TestCase(Contents.FrithiofsSaga)]
        [TestCase(Contents.Chinese)]
        [TestCase(Contents.DavidCopperfield)]
        [TestCase(Contents.LeonardoDaVinci)]
        [TestCase(Contents.WarAndPeace)]
        [TestCase(Contents.Tegner)]
        [TestCase(Contents.Iliad)]
        [TestCase(Contents.Arabic)]
        [TestCase(Contents.Bibeln)]
        [TestCase(Contents.Greek)]
        [TestCase(Contents.Japanese)]
        [TestCase(Contents.Russian)]
        public void IntegrityRows(string name)
        {
            IntegrityByRows(name, 0);
        }

        private void IntegrityByRows(string name, int numberOfLines)
        {
            List<string> rawstrings = Contents.Load(name, numberOfLines);

            foreach (var rawstring in rawstrings)
            {
                var stringette = Stringette.New(rawstring);
                Assert.AreEqual(rawstring, stringette.ToString());
            }
        }
        
        [TestCase(1, Contents.Strindberg)]
        [TestCase(1, Contents.Chinese)]
        [TestCase(1, Contents.Bibeln)]
        [TestCase(2, Contents.Strindberg)]
        [TestCase(2, Contents.Chinese)]
        [TestCase(2, Contents.Bibeln)]
        [TestCase(5, Contents.Strindberg)]
        [TestCase(5, Contents.Chinese)]
        [TestCase(5, Contents.Bibeln)]
        [TestCase(10, Contents.Strindberg)]
        [TestCase(10, Contents.Chinese)]
        [TestCase(10, Contents.Bibeln)]
        [TestCase(15, Contents.Strindberg)]
        [TestCase(15, Contents.Chinese)]
        [TestCase(15, Contents.Bibeln)]
        public void MultiThreading(int numberOfThreads, string content)
        {
            List<Thread> threads = new List<Thread>(numberOfThreads);

            for (int i = 0; i < numberOfThreads; i++)
            {
                Thread thread = new Thread(() =>
                {
                    IntegrityByRows(content, 50000);
                });

                thread.Start();
                threads.Add(thread);
            }

            for (int i = 0; i < numberOfThreads; i++)
            {
                threads[i].Join();
            }
        }

        [Test]
        public void Equals()
        {
            string value = "This is a string";

            var stringetteA = Stringette.New(value);
            var stringetteB = Stringette.New(value);

            Assert.AreEqual(stringetteA, stringetteB);

            string value2 = "This is also a string";

            var stringetteC = Stringette.New(value2);

            Assert.AreNotEqual(stringetteA, stringetteC);
            Assert.That(stringetteA.Equals(value));
        }

        [TestCase("This", "is", false, true)]
        [TestCase("This", "IS", true, true)]
        [TestCase("This", "Not", true, false)]
        [TestCase("This is a string containing stuff", "a string", false, true)]
        [TestCase("This is a string not containing stuff", "Noncontaining", false, false)]
        [TestCase("This is a string containing stuff", "A STRING", true, true)]
        [TestCase("This is a string not containing stuff", "A NOT STRING", true, false)]
        public void Contains(string stringette, string contain, bool ignoreCase, bool expected)
        {
            Stringette ette = Stringette.New(stringette);
            Assert.AreEqual(expected, ette.Contains(contain, ignoreCase));
        }

        [TestCase("This", "is", false, 2)]
        [TestCase("This", "IS", true, 2)]
        [TestCase("This", "Not", true, -1)]
        [TestCase("This is a string containing stuff", "this", true, 0)]
        [TestCase("This is a string containing stuff", "a string", false, 8)]
        [TestCase("This is a string not containing stuff", "a not string", false, -1)]
        [TestCase("This is a string containing stuff", "CONTAINING", true, 17)]
        [TestCase("This is a string not containing stuff", "A NOT STRING", true, -1)]
        public void IndexOf(string stringette, string contain, bool ignoreCase, int expected)
        {
            Stringette ette = Stringette.New(stringette);
            Assert.AreEqual(expected, ette.IndexOf(contain, ignoreCase));
        }

        [TestCase(Contents.Tegner, "svenska stålet", true)]
        [TestCase(Contents.Tegner, "segerfana", true)]
        [TestCase(Contents.Tegner, "Ärans och hjältarnas språk!", true)]
        [TestCase(Contents.Tegner, "English words", false)]
        public void IndexOfBig(string contentName, string test, bool shouldExist)
        {
            var stringContent = Contents.Load(contentName);
            Stringette ette = Stringette.New(stringContent);

            int stringIndex = stringContent.IndexOf(test, StringComparison.Ordinal);
            int stringetteIndex = ette.IndexOf(test);

            if (shouldExist)
            {
                Assert.AreEqual(stringIndex, stringetteIndex, "IndexOf not as expected");
                var etteSubstring = ette.Substring(stringetteIndex, test.Length);
                Assert.AreEqual(test, etteSubstring, "Substring not as expected");
            }
            else
            {
                Assert.AreEqual(-1, stringetteIndex, "IndexOf not as expected");
            }
        }

        [Test]
        public void Casting()
        {
            string testString = "Stringette is the coolest class";
            
            Stringette ette = (Stringette)testString;

            testString = "This is another string";

            testString = ette;

            Assert.AreEqual("Stringette is the coolest class", testString);
        }

        [TestCase("This is a string which is long", 30)]
        [TestCase("Another string with a certain length", 36)]
        [TestCase("", 0)]
        public void Length(string value, int expectedLength)
        {
            Stringette ette = Stringette.New(value);
            Assert.AreEqual(ette.Length, expectedLength);
        }
    }
}
