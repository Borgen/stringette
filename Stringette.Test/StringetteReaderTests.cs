﻿using NUnit.Framework;
using Stringette.Stringettes;
using Stringette.Test.Content;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stringette.Test
{
    [TestFixture]
    public class StringetteReaderTests
    {
        [TestCase(Contents.Strindberg)]
        [TestCase(Contents.Shakespeare)]
        [TestCase(Contents.Miserables)]
        [TestCase(Contents.KingJamesBible)]
        [TestCase(Contents.FrithiofsSaga)]
        [TestCase(Contents.Chinese)]
        [TestCase(Contents.DavidCopperfield)]
        [TestCase(Contents.LeonardoDaVinci)]
        [TestCase(Contents.WarAndPeace)]
        [TestCase(Contents.Tegner)]
        [TestCase(Contents.Iliad)]
        [TestCase(Contents.Arabic)]
        [TestCase(Contents.Bibeln)]
        [TestCase(Contents.Greek)]
        [TestCase(Contents.Japanese)]
        [TestCase(Contents.Russian)]
        public void ReadOne(string name)
        {
            string rawstring = Contents.Load(name);
            
            StringReader realReader = new StringReader(rawstring);
            StringetteReader stringetteReader = new StringetteReader(Stringette.New(rawstring));

            while (true)
            {
                int realResult = realReader.Read();
                int stringetteResult = stringetteReader.Read();

                if (realResult != stringetteResult)
                    Assert.Fail();

                if (realResult == -1)
                    break;
            }
        }

        [TestCase(Contents.Shakespeare, 128)]
        [TestCase(Contents.Shakespeare, 2 * 128)]
        [TestCase(Contents.Shakespeare, 4 * 128)]
        [TestCase(Contents.Shakespeare, 8 * 128)]
        [TestCase(Contents.Shakespeare, 16 * 128)]
        [TestCase(Contents.Shakespeare, 32 * 128)]
        public void ReadOneWithBuffer(string name, int bufferSize)
        {
            string rawstring = Contents.Load(name);

            StringReader realReader = new StringReader(rawstring);
            StringetteReader stringetteReader = new StringetteReader(Stringette.New(rawstring));

            while (true)
            {
                int realResult = realReader.Read();
                int stringetteResult = stringetteReader.Read();

                if (realResult != stringetteResult)
                    Assert.Fail();

                if (realResult == -1)
                    break;
            }
        }

        [TestCase(Contents.KingJamesBible, 128)]
        [TestCase(Contents.KingJamesBible, 2 * 128)]
        [TestCase(Contents.KingJamesBible, 4 * 128)]
        [TestCase(Contents.KingJamesBible, 8 * 128)]
        [TestCase(Contents.KingJamesBible, 16 * 128)]
        [TestCase(Contents.KingJamesBible, 32 * 128)]
        public void ReadOneWithBufferAndEveryLine(string name, int bufferSize)
        {
            List<string> rawstrings = Contents.Load(name, 0);

            for(int i = 0; i < rawstrings.Count; i++)
            {
                StringReader realReader = new StringReader(rawstrings[i]);
                StringetteReader stringetteReader = new StringetteReader(Stringette.New(rawstrings[i]));

                while (true)
                {
                    int realResult = realReader.Read();
                    int stringetteResult = stringetteReader.Read();

                    if (realResult != stringetteResult)
                        Assert.Fail();

                    if (realResult == -1)
                        break;
                }
            }
        }

        [TestCase(Contents.Strindberg)]
        [TestCase(Contents.Shakespeare)]
        [TestCase(Contents.Miserables)]
        [TestCase(Contents.KingJamesBible)]
        [TestCase(Contents.FrithiofsSaga)]
        [TestCase(Contents.Chinese)]
        [TestCase(Contents.DavidCopperfield)]
        [TestCase(Contents.LeonardoDaVinci)]
        [TestCase(Contents.WarAndPeace)]
        [TestCase(Contents.Tegner)]
        [TestCase(Contents.Iliad)]
        [TestCase(Contents.Arabic)]
        [TestCase(Contents.Bibeln)]
        [TestCase(Contents.Greek)]
        [TestCase(Contents.Japanese)]
        [TestCase(Contents.Russian)]
        public void ReadLines(string name)
        {
            string rawstring = Contents.Load(name);

            StringReader realReader = new StringReader(rawstring);
            StringetteReader stringetteReader = new StringetteReader(Stringette.New(rawstring));

            while (true)
            {
                string realResult = realReader.ReadLine();
                string stringetteResult = stringetteReader.ReadLine();

                if (realResult != stringetteResult)
                    Assert.Fail();

                if (realResult == null)
                    break;
            }
        }

        [TestCase(Contents.Strindberg)]
        [TestCase(Contents.Shakespeare)]
        [TestCase(Contents.Miserables)]
        [TestCase(Contents.KingJamesBible)]
        [TestCase(Contents.FrithiofsSaga)]
        [TestCase(Contents.Chinese)]
        [TestCase(Contents.DavidCopperfield)]
        [TestCase(Contents.LeonardoDaVinci)]
        [TestCase(Contents.WarAndPeace)]
        [TestCase(Contents.Tegner)]
        [TestCase(Contents.Iliad)]
        [TestCase(Contents.Arabic)]
        [TestCase(Contents.Bibeln)]
        [TestCase(Contents.Greek)]
        [TestCase(Contents.Japanese)]
        [TestCase(Contents.Russian)]
        public void ReadToEnd(string name)
        {
            string rawstring = Contents.Load(name);

            StringReader realReader = new StringReader(rawstring);
            StringetteReader stringetteReader = new StringetteReader(Stringette.New(rawstring));

            string realResult = realReader.ReadToEnd();
            string stringetteResult = stringetteReader.ReadToEnd();
            
            if (realResult != stringetteResult)
                Assert.Fail();
        }
    }
}
