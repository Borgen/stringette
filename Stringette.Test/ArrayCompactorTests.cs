﻿using NUnit.Framework;
using System;
using Stringette.Compactor.Generics;

namespace Stringette.Test
{
    [TestFixture]
    public class ArrayCompactorTests
    {
        [TestCase(100, 1000, int.MinValue, int.MaxValue)]
        [TestCase(100, 1000, 0, int.MaxValue)]
        [TestCase(100, 1000, int.MinValue, 0)]
        [TestCase(100, 1000, 0, 350)]
        [TestCase(10000, 10, 0, 350)]
        [TestCase(10000, 100, int.MinValue, int.MaxValue)]
        public void TestGenericCompactorWithRandom(int iterations, int arraySize, int minValue, int maxValue)
        {
            Random random = new Random();
            IntArrayCompactor compactor = new IntArrayCompactor();

            for (int i = 0; i < iterations; i++)
            {
                int[] expected = new int[arraySize];

                for (int t = 0; t < arraySize; t++)
                {
                    expected[t] = random.Next(minValue, maxValue);
                }

                ulong[] compactedData = compactor.Compact(expected);
                int[] uncompactedData = compactor.Uncompact(compactedData);

                for (int t = 0; t < expected.Length; t++)
                {
                    Assert.AreEqual(expected[t], uncompactedData[t], $"Uncompacted data was not the same as compacted! i: {i}, t: {t}");
                }
            }
        }

        [Test]
        public void TestGenericCompactor()
        {
            Random random = new Random();
            IntArrayCompactor compactor = new IntArrayCompactor();

            int[] expected = new[] { -1 };
            ulong[] compactedData = compactor.Compact(expected);
            int[] uncompactedData = compactor.Uncompact(compactedData);

            Assert.AreEqual(expected[0], uncompactedData[0], $"Uncompacted data was not the same as compacted!");
        }
    }
}
