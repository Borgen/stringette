﻿using Stringette.Stringettes;
using System;
using System.Collections.Generic;

namespace Stringette
{
    internal class StringetteFactory
    {
        public static readonly List<StringetteFactory> Factories = new List<StringetteFactory>()
        {
            new StringetteFactory(StringStringette.EstimatedFootprint,  s => new StringStringette(s)),
            new StringetteFactory(GlobalIndexingCompactStringette.EstimatedFootprint,  s => new GlobalIndexingCompactStringette(s)),
            new StringetteFactory(SelfIndexingCompactStringette.EstimatedFootprint,  s => new SelfIndexingCompactStringette(s))
        };

        internal static Stringette Create(string value)
        {
            long lowestFootprint = long.MaxValue;
            Func<string, Stringette> lowestFactoryFunc = null;

            for (int i = 0; i < Factories.Count; i++)
            {
                long footprint = Factories[i].FootPrintFunc(value);

                if (footprint < lowestFootprint)
                {
                    lowestFootprint = footprint;
                    lowestFactoryFunc = Factories[i].FactoryFunc;
                }
            }

            return lowestFactoryFunc?.Invoke(value);
        }
        
        public StringetteFactory(Func<string, long> footPrintFunc, Func<string, Stringette> factoryFunc)
        {
            FootPrintFunc = footPrintFunc;
            FactoryFunc = factoryFunc;
        }
        
        public Func<string, long> FootPrintFunc { get; }
        
        public Func<string, Stringette> FactoryFunc { get; }
    }
}
