﻿
namespace Stringette.Compactor.Generics
{
    public class CharArrayCompactor : BaseArrayCompactor<char>
    {
        protected override char GetLargestValue(char[] data) => (char)ArrayHelper.GetHighestValue(data);

        protected override char ToTee(ulong value) => (char)value;

        protected override ulong ToUInt(char value) => value;
    }
}
