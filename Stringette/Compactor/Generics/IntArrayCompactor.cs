﻿
namespace Stringette.Compactor.Generics
{
    public class IntArrayCompactor : BaseArrayCompactor<int>
    {
        protected override int GetLargestValue(int[] data) => (int)ArrayHelper.GetHighestValue(data);

        protected override int ToTee(ulong value) => (int)value;

        protected override ulong ToUInt(int value) => (uint)value;
    }
}
