﻿using System;
using System.Collections.Generic;

namespace Stringette.Compactor.Generics
{
    public abstract class BaseArrayCompactor<T> : IArrayCompactor<T>
    {
        //6 bits will allow for up to 63 bit entry size (we are limited to 32 however by the 
        //fact that we are packing ints)
        protected const int BitsPerEntryHeaderSize = 6;

        //As we are limited to the Array.Length property which is an int and we know it allways 
        //will be positive we can limit ourself to only store 31 bit.
        protected const int CountHeaderSize = 31;

        const int AllEntires = -1;

        #region Public interface

        /// <summary>
        /// Loss-less, compaction of the given integer array. Returns an uint buffer containing the compacted data.
        /// </summary>
        public ulong[] Compact(T[] data)
        {
            //Create bitpack out of the data
            BitPack pack = new BitPack();
            pack.Setup(data, this);

            //Allocate new buffer for compacted data
            pack.Allocate();

            //Write the BitPack header to the buffer
            WritePack(pack);

            return pack.Buffer;
        }

        /// <summary>
        /// Get the number of compacted elements in a buffer
        /// </summary>
        public int GetCount(ulong[] compactedData)
        {
            return (int)BitHelper.ReadData(compactedData, BitsPerEntryHeaderSize, CountHeaderSize);
        }

        /// <summary>
        /// Uncompacts the given buffer and returns the original data.
        /// </summary>
        public T[] Uncompact(ulong[] compactedData)
        {
            return Uncompact(compactedData, 0, AllEntires);
        }

        /// <summary>
        /// Uncompacts the given buffer starting at indexFrom and length number of entries. 
        /// If length is -1 it will read to the end.
        /// </summary>
        public T[] Uncompact(ulong[] compactedData, int indexFrom, int length)
        {
            BitPack header = ReadHeader(compactedData);

            if (length == AllEntires)
            {
                length = header.Count;
            }
            else
            {
                if (indexFrom + length > header.Count)
                {
                    throw new IndexOutOfRangeException("indexFrom + length are bigger than Count");
                }
            }

            T[] rawData = new T[length];

            for (int i = 0; i < length; i++)
            {
                rawData[i] = ToTee(BitHelper.ReadData(compactedData, header.GetBitIndex(indexFrom + i), header.BitsPerEntry));
            }

            return rawData;
        }

        /// <summary>
        /// Forwards to <see cref="ToEnumerable(ulong[], int)"/> starting at index 0.
        /// </summary>
        public IEnumerable<T> ToEnumerable(ulong[] compactedData)
        {
            return ToEnumerable(compactedData, 0);
        }

        /// <summary>
        /// Returns the original data of a compacted buffer as an IEnumerable. Starting at the supplied index.
        /// </summary>
        public IEnumerable<T> ToEnumerable(ulong[] compactedData, int indexFrom)
        {
            BitPack pack = ReadHeader(compactedData);

            if (indexFrom >= pack.Count)
            {
                throw new IndexOutOfRangeException("indexFrom outside range of compacted data");
            }

            for (int i = indexFrom; i < pack.Count; i++)
            {
                yield return ToTee(BitHelper.ReadData(pack.Buffer, pack.GetBitIndex(i), pack.BitsPerEntry));
            }
        }

        /// <summary>
        /// Calculates the memory footprint of a compacted buffer containing 'length' entries. Return value in bytes.
        /// </summary>
        public long GetEstimatedFootprint(T largestEntry, int length)
        {
            return BitHelper.UlongRequired(GetBitsRequiered(ToUInt(largestEntry)) * length + BitsPerEntryHeaderSize + CountHeaderSize) * sizeof(ulong);
        }

        #endregion

        #region BitPack

        private class BitPack
        {
            public int BitsPerEntry;
            public int Count;
            public T[] RawData;
            public ulong[] Buffer;

            public BitPack()
            {
            }

            public void Setup(T[] data, BaseArrayCompactor<T> comp)
            {
                BitsPerEntry = comp.GetBitsRequiered(data);
                Count = data.Length;
                RawData = data;
                Buffer = null;
            }

            public long TotalBitsRequired => BitsPerEntryHeaderSize + CountHeaderSize + Count * BitsPerEntry;

            public long GetBitIndex(int index)
            {
                return BitsPerEntryHeaderSize + CountHeaderSize + index * BitsPerEntry;
            }

            public ulong[] Allocate()
            {
                Buffer = BitHelper.Allocate(TotalBitsRequired);
                return Buffer;
            }
        }

        private void WritePack(BitPack bitPack)
        {
            //Write the BitPack header to the buffer
            WriteHeader(bitPack);

            //Write each data entry to the buffer
            for (int i = 0; i < bitPack.RawData.Length; i++)
            {
                BitHelper.WriteData(bitPack.Buffer, ToUInt(bitPack.RawData[i]), bitPack.GetBitIndex(i), bitPack.BitsPerEntry);
            }
        }

        private void WriteHeader(BitPack header)
        {
            BitHelper.WriteData(header.Buffer, (ulong)header.BitsPerEntry, 0, BitsPerEntryHeaderSize);
            BitHelper.WriteData(header.Buffer, (ulong)header.Count, BitsPerEntryHeaderSize, CountHeaderSize);
        }

        private BitPack ReadHeader(ulong[] data)
        {
            BitPack header = new BitPack
            {
                BitsPerEntry = (int)BitHelper.ReadData(data, 0, BitsPerEntryHeaderSize),
                Count = (int)BitHelper.ReadData(data, BitsPerEntryHeaderSize, CountHeaderSize),
                Buffer = data
            };
            
            return header;
        }

        #endregion

        private int GetBitsRequiered(ulong largestValue)
        {
            if (largestValue == 0)
                return 1;

            for (int i = BitHelper.UlongSize; i >= 0; i--)
            {
                if (BitHelper.Masks[i] < largestValue)
                {
                    return i + 1;
                }
            }

            throw new ApplicationException("Could not determine the proper bit size");
        }

        private int GetBitsRequiered(T[] data)
        {
            return GetBitsRequiered(ToUInt(GetLargestValue(data)));
        }

        #region Abstracts

        protected abstract T GetLargestValue(T[] data);

        protected abstract T ToTee(ulong value);

        protected abstract ulong ToUInt(T value);
        
        #endregion
    }
}
