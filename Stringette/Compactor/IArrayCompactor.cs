﻿using System.Collections.Generic;

namespace Stringette.Compactor
{
    /// <summary>
    /// Interface for an ArrayCompactor. This should be used to define a class that can compact a given Array of T[] to a smaller uint[].
    /// </summary>
    public interface IArrayCompactor<T>
    {
        /// <summary>
        /// Loss-less, compaction of the given integer array. Returns an uint buffer containing the compacted data.
        /// </summary>
        ulong[] Compact(T[] data);

        /// <summary>
        /// Get the number of compacted elements in a buffer
        /// </summary>
        int GetCount(ulong[] compactedData);

        /// <summary>
        /// Uncompacts the given buffer and returns the original data.
        /// </summary>
        T[] Uncompact(ulong[] compactedData);

        /// <summary>
        /// Uncompacts the given buffer starting at indexFrom and length number of entries. 
        /// If length is -1 it will read to the end.
        /// </summary>
        T[] Uncompact(ulong[] compactedData, int indexFrom, int length);

        /// <summary>
        /// Forwards to <see cref="ToEnumerable(UintType[], int)"/> starting at index 0.
        /// </summary>
        IEnumerable<T> ToEnumerable(ulong[] compactedData);

        /// <summary>
        /// Returns the original data of a compacted buffer as an IEnumerable. Starting at the supplied index.
        /// </summary>
        IEnumerable<T> ToEnumerable(ulong[] compactedData, int indexFrom);

        /// <summary>
        /// Calculates the memory footprint of a compacted buffer containing 'length' entries. Return value in bytes.
        /// </summary>
        long GetEstimatedFootprint(T largestEntry, int length);
    }

}
