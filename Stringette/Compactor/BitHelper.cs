﻿
using System;

namespace Stringette.Compactor
{
    internal static class BitHelper
    {
        public const int UlongSize = sizeof(ulong) * 8;
        public const int ModulusMask = UlongSize - 1;
        public const int DivisionShift = 6;

        #region Bitshifting magic

        /*HERE BE DRAGONS! BEWARE!
                        ^    ^
                       / \  //\
         |\___/|      /   \//  .\
         /O  O  \__  /    //  | \ \
        /     /  \/_/    //   |  \  \
        @___@'    \/_   //    |   \   \ 
           |       \/_ //     |    \    \ 
           |        \///      |     \     \ 
          _|_ /   )  //       |      \     _\
         '/,_ _ _/  ( ; -.    |    _ _\.-~        .-~~~^-.
         ,-{        _      `-.|.-~-.           .~         `.
          '/\      /                 ~-. _ .-~      .-~^-.  \
             `.   {            }                   /      \  \
           .----~-.\        \-'                 .~         \  `. \^-.
          ///.----..>    c   \             _ -~             `.  ^-`   ^-_
            ///-._ _ _ _ _ _ _}^ - - - - ~                     ~--,   .-~*/

        internal static void WriteData(ulong[] buffer, ulong value, long startBit, int length)
        {
            int indexFrom = GetIndexAtBit(startBit);
            int indexTo = GetIndexAtBit(startBit + length);
            (int bitsInLower, int bitsInHigher) = GetBitsPerUlong(startBit, length);

            //All bits should be written to the same ulong
            if (indexFrom == indexTo)
            {
                buffer[indexFrom] |= value << (bitsInLower - length);
            }
            //We have to write to both the higher and the lower ulong
            else
            {
                if (bitsInLower > 0)
                    buffer[indexFrom] |= value >> bitsInHigher;
                if (bitsInHigher > 0)
                    buffer[indexTo] |= value << (UlongSize - bitsInHigher);
            }
        }

        internal static ulong ReadData(ulong[] buffer, long startBit, int length)
        {
            if (length == 0)
                return 0;

            (int bitsInLower, int bitsInHigher) = GetBitsPerUlong(startBit, length);

            //If bitsInRight is 0 or less we know that all bits are in the left(lower) index
            if (bitsInHigher <= 0)
            {
                int rightShift = bitsInLower - length;
                int index = GetIndexAtBit(startBit);
                
                return Mask(buffer[index] >> rightShift, length);
            }

            //We have bits in two ulongs and read it from them recursivly and then Bitwise OR them
            ulong bigData = ReadData(buffer, startBit, bitsInLower);
            ulong smallData = ReadData(buffer, startBit + bitsInLower, bitsInHigher);

            return ((bigData << bitsInHigher) | smallData);
        }

        private static (int bitsInLower, int bitsInHigher) GetBitsPerUlong(long startBit, int length)
        {
            int bitsInLower = UlongSize - (int)(startBit & ModulusMask);
            int bitsInHigher = length - bitsInLower;
            return (bitsInLower, bitsInHigher);
        }

        private static int GetIndexAtBit(long bitIndex)
        {
            return (int)(bitIndex >> DivisionShift);
        }

        #endregion

        #region Masking

        internal static ulong Mask(ulong source, int masksize)
        {
            return source & Masks[masksize];
        }

        internal static readonly ulong[] Masks = CreateMasks();

        private static ulong[] CreateMasks()
        {
            ulong[] masks = new ulong[UlongSize + 1];

            masks[0] = 0;

            for (int i = 0; i < UlongSize; i++)
            {
                ulong value = 1;

                for (int t = 0; t < i; t++)
                {
                    value = value << 1;
                    value += 1;
                }

                masks[i + 1] = value;
            }

            return masks;
        }

        #endregion
        
        internal static long UlongRequired(long bits)
        {
            long uintsRequired = bits / UlongSize;

            if ((bits & ModulusMask) > 0)
                uintsRequired += 1;

            return uintsRequired;
        }

        internal static ulong[] Allocate(long bits)
        {
            return new ulong[UlongRequired(bits)];
        }
    }
}
