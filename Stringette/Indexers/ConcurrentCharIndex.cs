﻿using System;
using System.Collections.Concurrent;

namespace Stringette.Indexers
{
    /// <summary>
    /// This class is responsible for storing the master table of character to index mapping
    /// for the stringette. 
    /// </summary>
    internal class ConcurrentCharIndex : ICharIndex
    {
        private const int DefaultStartingSize = 512;
        private const int DefaultConcurrencyLevel = 10;

        private readonly AddOnlyList<char> _chars;
        private readonly ConcurrentDictionary<char, int> _indecies;
        
        /// <summary>
        /// Forwards to <see cref="ConcurrentCharIndex(int)"/> with a default estimated number of characters of 512.
        /// </summary>
        public ConcurrentCharIndex() : this(DefaultStartingSize)
        {
        }

        /// <summary>
        /// This constructor will allocate enough space for allowing the estCharacters.
        /// </summary>
        public ConcurrentCharIndex(int estCharacters)
        {
            _chars = new AddOnlyList<char>(estCharacters);
            _indecies = new ConcurrentDictionary<char, int>(DefaultConcurrencyLevel, estCharacters);
        }

        /// <summary>
        /// Adds the character to the CharIndex and returns the index of that character.
        /// </summary>
        public int Index(char character)
        {
            return _indecies.GetOrAdd(character, c => _chars.Add(c));
        }

        /// <summary>
        /// Get all characters in the index as a new array.
        /// </summary>
        public ArraySegment<char> GetChars()
        {
            return _chars.ToArray();
        }

        /// <summary>
        /// Returns the number of characters that has been indexed.
        /// </summary>
        public int GetCharCount()
        {
            return _chars.Count;
        }

        /// <summary>
        /// Clears the underlying collections. Note: Could be dangerous to do in normal operations as 
        /// it would delete the index of already existing stringettes, making those corrupt! 
        /// </summary>
        public void Reset()
        {
            _indecies.Clear();
            _chars.Clear();
        }
    }
}
