﻿using System;

namespace Stringette.Indexers
{
    /// <summary>
    /// This interface defines the behaviour necessary for the compacted stringette to map characters to smaller indecies.
    /// </summary>
    public interface ICharIndex
    {
        /// <summary>
        /// Adds the character to the CharIndex and returns the index of that character.
        /// </summary>
        int Index(char character);

        /// <summary>
        /// Get all characters in the index as a new array.
        /// </summary>
        ArraySegment<char> GetChars();

        /// <summary>
        /// Returns the number of characters that has been indexed.
        /// </summary>
        int GetCharCount();

        /// <summary>
        /// Clears the underlying data. Note: Could be dangerous to do in normal operations as 
        /// it would delete the index of already existing stringettes, making those corrupt! 
        /// </summary>
        void Reset();
    }
}