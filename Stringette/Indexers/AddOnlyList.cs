﻿using System;

namespace Stringette.Indexers
{
    internal class AddOnlyList<T>
    {
        private readonly object lockObject = new object();
        private const int DefaultInitialSize = 4;

        private volatile T[] _data;
        private volatile int _count;
        
        public int Count => _count;

        public AddOnlyList() : this(DefaultInitialSize)
        {
        }

        public AddOnlyList(int initialSize)
        {
            _data = new T[initialSize];
            _count = 0;
        }

        public int Add(T value)
        { 
            lock (lockObject)
            {
                if (_count == _data.Length)
                {
                    Expand();
                }

                int newIndex = _count++;
                _data[newIndex] = value;
                return newIndex;
            }
        }

        private void Expand()
        {
            T[] newData = new T[_data.Length * 2];
            Array.Copy(_data, newData, _data.Length);
            _data = newData;
        }
        
        internal ArraySegment<T> ToArray()
        {
            return new ArraySegment<T>(_data, 0, _count);
        }

        internal void Clear()
        {
            lock (lockObject)
            {
                _count = 0;
                Array.Clear(_data, 0, _data.Length);
            }
        }
    }
}
