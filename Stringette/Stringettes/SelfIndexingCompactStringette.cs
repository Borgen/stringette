﻿using System;
using System.Collections.Generic;
using Stringette.Compactor;
using Stringette.Compactor.Generics;

namespace Stringette.Stringettes
{
    internal class SelfIndexingCompactStringette  : CompactStringette
    {
        public static IArrayCompactor<char> CharArrayCompactor = new CharArrayCompactor();

        private ulong[] _compactedChars;
        
        internal SelfIndexingCompactStringette(string stringValue) : base(stringValue)
        {
        }

        protected override ArraySegment<char> GetChars()
        {
            var chars = CharArrayCompactor.Uncompact(_compactedChars);
            return new ArraySegment<char>(chars, 0, chars.Length);
        }

        protected override int[] GetIndecies(string stringValue)
        {
            (char[] chars, int[] indecies) = FindUniqueChars(stringValue);
            _compactedChars = CharArrayCompactor.Compact(chars);
            return indecies;
        }
        
        private (char[] uniqueChars, int[] indecies) FindUniqueChars(string stringValue)
        {
            Dictionary<char, int> charCache = new Dictionary<char, int>(50);
            List<char> uniqueChars = new List<char>(50);
            int[] indecies = new int[stringValue.Length];

            for (int i = 0; i < stringValue.Length; i++)
            {
                char character = stringValue[i];

                if (!charCache.ContainsKey(character))
                {
                    uniqueChars.Add(character);
                    charCache.Add(character, uniqueChars.Count - 1);
                }

                indecies[i] = charCache[character];
            }

            return (uniqueChars.ToArray(), indecies);
        }

        internal static long EstimatedFootprint(string value)
        {
            //Since this estimation is CPU-intensive we hide behind the PrioritizeCompression flag.
            if (!PrioritizeCompression)
                return long.MaxValue;

            char largestChar = char.MinValue;
            HashSet<char> chars = new HashSet<char>();
            
            for (int i = 0; i < value.Length; i++)
            {
                if (!chars.Contains(value[i]))
                {
                    largestChar = (char)Math.Max(largestChar, value[i]);
                    chars.Add(value[i]);
                }
            }
            
            long indexSize = IntArrayCompactor.GetEstimatedFootprint(chars.Count, value.Length);
            long charsSize = CharArrayCompactor.GetEstimatedFootprint(largestChar, chars.Count);

            return indexSize + charsSize;
        }
    }
}
