﻿using System;
using Stringette.Indexers;

namespace Stringette.Stringettes
{
    public class GlobalIndexingCompactStringette : CompactStringette
    {
        public static readonly ICharIndex CharIndex = new ConcurrentCharIndex();
        
        public GlobalIndexingCompactStringette(string stringValue) : base(stringValue)
        {
        }

        protected override ArraySegment<char> GetChars()
        {
            return CharIndex.GetChars();
        }

        protected override int[] GetIndecies(string stringValue)
        {
            //Go through all the chars and check them in the indexer
            int[] indecies = new int[stringValue.Length];

            for (int i = 0; i < indecies.Length; i++)
            {
                indecies[i] = CharIndex.Index(stringValue[i]);
            }

            return indecies;
        }
        
        internal static long EstimatedFootprint(string value)
        {
            int numberOfChars = CharIndex.GetCharCount();
            return IntArrayCompactor.GetEstimatedFootprint(numberOfChars, value.Length);
        }
    }
}
