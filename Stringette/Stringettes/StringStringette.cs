﻿using System;

namespace Stringette.Stringettes
{
    class StringStringette : Stringette
    {
        private readonly string _stringData;

        public override int Length => _stringData.Length;

        internal StringStringette(string stringValue)
        {
            _stringData = stringValue;
        }

        public override int GetHashCode()
        {
            return _stringData.GetHashCode();
        }

        public override bool Equals(Stringette obj)
        {
            if (obj == null)
                return false;

            var stringette = obj as StringStringette;

            if (stringette == null)
            {
                return ToString().Equals(obj.ToString());
            }

            return _stringData.Equals(stringette._stringData);
        }

        public override string ToString()
        {
            return _stringData;
        }

        public override int IndexOf(string value, int index, bool ignoreCase)
        {
            return _stringData.IndexOf(value, index, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal);
        }

        internal override string SubstringProtected(int startindex, int length)
        {
            return _stringData.Substring(startindex, length);
        }
        
        internal override char[] GetCharArray(int startindex, int length)
        {
            return _stringData.ToCharArray(startindex, length);
        }

        internal static long EstimatedFootprint(string value)
        {
            return value.Length * sizeof(char);
        }
    }
}
