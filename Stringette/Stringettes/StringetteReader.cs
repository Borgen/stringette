﻿using System;
using System.IO;

namespace Stringette.Stringettes
{
    /// <summary>
    /// This reader will allow reading a stringette without materializing the entire string.
    /// 
    /// Could for example be used for serializing into a I/O stream.
    /// </summary>
    public class StringetteReader : TextReader
    {
        /// <summary>
        /// The default size of the internal buffer
        /// </summary>
        public const int DefaultBufferSize = 4 * 1024;

        private readonly ReaderBuffer _buffer;
        private int _currentIndex;

        /// <summary>
        /// Will create a new StringetteReader reading from the supplied Stringette. 
        /// Will use the default buffer size.
        /// </summary>
        /// <param name="stringette">Stringette to read from.</param>
        public StringetteReader(Stringette stringette) : this(stringette, DefaultBufferSize)
        {
        }

        /// <summary>
        /// Will create a new StringetteReader reading from the supplied Stringette. 
        /// Creating a buffer of supplied size.
        /// </summary>
        /// <param name="stringette">Stringette to read from.</param>
        /// <param name="bufferSize">Size of internal buffer.</param>
        public StringetteReader(Stringette stringette, int bufferSize)
        {
            _currentIndex = 0;
            _buffer = new ReaderBuffer(stringette, bufferSize);
        }
        
        /// <summary>
        /// Reads one character from the stringette and advances the pointer.
        /// </summary>
        /// <returns>The character value if any. If there is no more data to read it returns -1.</returns>
        public override int Read() => ReadOne(true);

        /// <summary>
        /// Read one character from the stringette without advancing the pointer.
        /// </summary>
        /// <returns>The character value if any. If there is no more data to read it returns -1.</returns>
        public override int Peek() => ReadOne(false);

        private int ReadOne(bool advanceIndex)
        {
            if (_currentIndex < _buffer.Length)
            {
                int result = _buffer.Get(_currentIndex);

                if (advanceIndex)
                    _currentIndex++;

                return result;
            }

            return -1;
        }
        
        private class ReaderBuffer
        {
            private char[] _buffer;
            private int _count;
            private int _startingIndex;
            private Stringette _source;

            internal int Length { get; private set; }

            internal ReaderBuffer(Stringette source, int bufferSize)
            {
                _source = source;
                Length = source.Length;
                _count = 0;
                _startingIndex = 0;
                _buffer = new char[bufferSize];
            }

            internal void EnsureIndex(int index)
            {
                if (index >= _startingIndex + _count)
                {
                    int nextIndex = _startingIndex + _count;
                    int readCount = Math.Min(_buffer.Length, Length - nextIndex);

                    char[] preBuffer = _source.GetCharArray(nextIndex, readCount);
                    Array.Copy(preBuffer, _buffer, readCount);

                    _startingIndex = nextIndex;
                    _count = readCount;
                }
            }

            internal int Get(int index)
            {
                EnsureIndex(index);
                return _buffer[index - _startingIndex];
            }
        }
    }
}
