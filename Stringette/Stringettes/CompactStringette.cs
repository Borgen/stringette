﻿using Stringette.Compactor;
using Stringette.Compactor.Generics;
using System;

namespace Stringette.Stringettes
{
    public abstract class CompactStringette : Stringette
    {
        public static IArrayCompactor<int> IntArrayCompactor = new IntArrayCompactor();

        private readonly ulong[] _compactedData;

        public override int Length => IntArrayCompactor.GetCount(_compactedData);

        protected abstract ArraySegment<char> GetChars();

        protected abstract int[] GetIndecies(string stringValue);

        protected CompactStringette(string stringValue)
        {
            _compactedData = CompactString(stringValue);
        }
        
        private ulong[] CompactString(string stringValue)
        {
            return IntArrayCompactor.Compact(GetIndecies(stringValue));
        }

        public override bool Equals(Stringette obj)
        {
            if (obj == null)
                return false;

            var stringette = obj as CompactStringette;

            if (stringette == null)
            {
                return ToString().Equals(obj.ToString());
            }

            return ArrayHelper.ArraysAreEqual(_compactedData, stringette._compactedData);
        }

        public override int GetHashCode()
        {
            ulong[] array = _compactedData;
            ulong hashCode = (ulong)array.Length;

            for (int i = 0; i < array.Length; i++)
            {
                hashCode = unchecked(hashCode * 17 + array[i]);
            }

            return (int)hashCode;
        }

        public override string ToString()
        {
            int[] indecies = IntArrayCompactor.Uncompact(_compactedData);
            return ArrayToString(indecies, GetChars());
        }

        public override int IndexOf(string value, int startIndex, bool ignoreCase)
        {
            int index = 0;
            int matchCount = 0;
            ArraySegment<char> characters = GetChars();

            foreach (int uncompactedValue in IntArrayCompactor.ToEnumerable(_compactedData, startIndex))
            {
                char character = characters.Get(uncompactedValue);
                char char1 = ignoreCase ? char.ToUpperInvariant(character) : character;
                char char2 = ignoreCase ? char.ToUpperInvariant(value[matchCount]) : value[matchCount];

                if (char1 == char2)
                {
                    //The characters match, check the next letter
                    matchCount++;
                }
                else
                {
                    index += matchCount + 1;
                    matchCount = 0;
                }

                //We've have a match for the entire control string
                if (matchCount == value.Length)
                    return index;
            }
            
            return -1;
        }

        internal override string SubstringProtected(int startindex, int length)
        {
            int[] subData = IntArrayCompactor.Uncompact(_compactedData, startindex, length);
            return ArrayToString(subData, GetChars());
        }

        internal override char[] GetCharArray(int startindex, int length)
        {
            int[] subData = IntArrayCompactor.Uncompact(_compactedData, startindex, length);
            return ArrayToChars(subData, GetChars());
        }
        
        private string ArrayToString(int[] indecies, ArraySegment<char> chars)
        {
            return new string(ArrayToChars(indecies, chars));
        }

        private char[] ArrayToChars(int[] indecies, ArraySegment<char> chars)
        {
            int length = indecies.Length;
            char[] output = new char[length];

            for (int i = 0; i < length; i++)
            {
                output[i] = chars.Get(indecies[i]);
            }

            return output;
        }
    }
}
