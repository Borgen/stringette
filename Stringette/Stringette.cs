﻿using System;
using System.Diagnostics;

namespace Stringette
{
    /// <summary>
    /// Stringette is a light weight string class that is supposed to optimize the memory footprint of 
    /// the text while being quick to create and materialize.
    /// 
    /// If the string is short enough the stringette will use a System.String as the underlying data. 
    /// Given that, Stringette should be used when the strings are longer than 8 letters. And the longer
    /// the string the more potential benefit.
    /// 
    /// The stringette works best when there is a few number of unique characters in the string.
    /// 
    /// String comparison is most like the StringComparison.Ordinal or StringComparison.OrdinalIgnoreCase
    /// </summary>
    [DebuggerDisplay("Stringette, Length={Length}")]
    public abstract class Stringette : IEquatable<Stringette>
    {
        /// <summary>
        /// Should the Stringettes prioritize the level of compression or creation speed.
        /// </summary>
        public static bool PrioritizeCompression { get; set; } = false;

        #region Object overrides

        public abstract override int GetHashCode();

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var stringette = obj as Stringette;

            if (stringette == null)
            {
                if(obj is string)
                {
                    return ((string)obj).Equals(ToString());
                }

                return false;
            }

            return Equals(stringette);
        }
        
        public abstract bool Equals(Stringette other);
        
        public abstract override string ToString();

        #endregion

        #region String functions
        
        /// <summary>
        /// Forwards to <see cref="Contains(string, bool)"/> not ignoring case. 
        /// </summary>
        public bool Contains(string value)
        {
            return Contains(value, false);
        }

        /// <summary>
        /// Returns whether or not the Stringette contains the supplied string as a substring. The comparison 
        /// is like the StringComparison.Ordinal or StringComparison.OrdinalIgnoreCase, as it do not take 
        /// culture into consideration. Only the characters.
        /// </summary>
        public bool Contains(string value, bool ignoreCase)
        {
            return IndexOf(value, ignoreCase) > -1;
        }

        /// <summary>
        /// Forwards to <see cref="IndexOf(string, bool)"/> not ignoring case. 
        /// </summary>
        public int IndexOf(string value)
        {
            return IndexOf(value, false);
        }

        /// <summary>
        /// Forwards to <see cref="IndexOf(string, int, bool)"/> starting at index = 0. 
        /// </summary>
        public int IndexOf(string value, bool ignoreCase)
        {
            return IndexOf(value, 0, ignoreCase);
        }

        /// <summary>
        /// Returns the index of the first occurance of the supplied string in the Stringette.
        /// The comparison is like the StringComparison.Ordinal or StringComparison.OrdinalIgnoreCase, 
        /// as it do not take culture into consideration. Only the character data.
        /// </summary>
        public abstract int IndexOf(string value, int indexFrom, bool ignoreCase);

        /// <summary>
        /// Forwards to <see cref="Substring(int, int)"/>, returning all characters from index in the Stringette.
        /// </summary>
        public string Substring(int startindex)
        {
            return Substring(startindex, Length - startindex);
        }

        /// <summary>
        /// Returns a substring of the Stringette, starting from the given index and with the supplied length.
        /// </summary>
        public string Substring(int startindex, int length)
        {
            if(startindex + length > Length)
            {
                throw new IndexOutOfRangeException("Index of startIndex + length is outside the Stringettes range");
            }

            return SubstringProtected(startindex, length);
        }

        internal abstract string SubstringProtected(int startindex, int length);

        internal abstract char[] GetCharArray(int startindex, int length);

        /// <summary>
        /// Number of characters in this Stringette
        /// </summary>
        public abstract int Length { get; }

        #endregion

        #region Casting

        public static explicit operator Stringette(string s)
        {
            return New(s);
        }
        
        public static implicit operator string(Stringette s)
        {
            return s.ToString();
        }

        #endregion

        #region Creation
        
        /// <summary>
        /// Creates and returns a new Stringette containing the supplied string data.
        /// </summary>
        public static Stringette New(string s)
        {
            return StringetteFactory.Create(s);
        }

        #endregion 
    }
}
