﻿using System;
using System.Collections.Generic;

namespace Stringette
{
    internal static class ArrayHelper
    {
        public static T Get<T>(this ArraySegment<T> segment, int index)
        {
            return segment.Array[index];
        }

        public static uint GetHighestValue(int[] data)
        {
            if (data == null || data.Length == 0)
                return 0;
            
            uint highest = (uint)data[0];

            for (int i = 1; i < data.Length; i++)
            {
                highest = Math.Max(highest, (uint)data[i]);
            }

            return highest;
        }
        
        public static uint GetHighestValue(char[] data)
        {
            if (data == null || data.Length == 0)
                return 0;

            ushort highest = data[0];

            for (int i = 1; i < data.Length; i++)
            {
                highest = Math.Max(highest, data[i]);
            }

            return highest;
        }

        public static bool ArraysAreEqual<T>(T[] array1, T[] array2)
        {
            //if they are not of the same length they are not equal.
            if (array1.Length != array2.Length)
            {
                return false;
            }

            for (int i = 0; i < array1.Length; i++)
            {
                //Check if the compacted data is different in the two Stringettes
                if (!array1[i].Equals(array2[i]))
                    return false;
            }

            //All uints are the same, and thus equal :)
            return true;
        }

        public static void AddCapped<T>(this List<T> list, T value, int limit)
        {
            list.Add(value);
            if (list.Count > limit)
                list.RemoveAt(0);
        }
    }
}
