﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Stringette.Test.Content
{
    public static class Contents
    {
        public const string DavidCopperfield = "davidcopperfield";
        public const string FrithiofsSaga = "frithiof";
        public const string Iliad = "iliad";
        public const string KingJamesBible = "kingjamesbible";
        public const string LeonardoDaVinci = "leonardo";
        public const string Miserables = "miserables";
        public const string Shakespeare = "shakespeare";
        public const string Strindberg = "strindberg";
        public const string WarAndPeace = "warandpeace";
        public const string Tegner = "tegner";
        public const string Chinese = "chinese";
        public const string Arabic = "arabic";
        public const string Bibeln = "bibeln";
        public const string Greek = "greek";
        public const string Japanese = "japanese";
        public const string Russian = "russian";

        public static List<string> Load(string name, int numberOfLines)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = $"Stringette.Test.Content.{name}.txt";

            List<string> rawstrings = new List<string>();

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                string line;
                while ((line = reader.ReadLine()) != null
                    && (rawstrings.Count < numberOfLines || numberOfLines == 0))
                {
                    rawstrings.Add(line + Environment.NewLine);
                }
            }

            return rawstrings;
        }

        public static string Load(string name)
        {
            List<string> rows = Load(name, 0);
            return string.Join(Environment.NewLine, rows);
        }
    }
}
